package pillars;

public interface Renderer {
    String render(Message message);
}
