package pillars;

import java.util.List;

public class MessageRenderer implements Renderer {
    private final List<Renderer> subrenderers;

    public MessageRenderer() {
        subrenderers = List.of(new HeaderRenderer(), new BodyRenderer(), new FooterRenderer());
    }

    @Override
    public String render(Message message) {
        return subrenderers.stream()
                .map(renderer -> renderer.render(message))
                .reduce((s1, s2) -> s1 + s2)
                .orElse("<error>Ooops</error");
    }

    public List<Renderer> getSubrenderers() {
        return subrenderers;
    }
}