package pillars;

public class FooterRenderer implements Renderer {
    @Override
    public String render(Message message) {
        return String.format("<f>%s</f>", message.footer());
    }
}
