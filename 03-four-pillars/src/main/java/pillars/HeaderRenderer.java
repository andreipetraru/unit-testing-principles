package pillars;

public class HeaderRenderer implements Renderer {
    @Override
    public String render(Message message) {
        return String.format("<h>%s</h>", message.header());
    }
}
