package pillars;

public record Message(String header, String body, String footer) {
}
