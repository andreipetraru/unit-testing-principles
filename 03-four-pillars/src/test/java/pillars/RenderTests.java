package pillars;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class RenderTests {
    @Test
    void renderer_has_header_body_footer() {
        // given
        var renderer = new MessageRenderer();

        // when
        var renderers = renderer.getSubrenderers();

        // then
        assertThat(renderers).hasSize(3);
        assertThat(renderers.get(0).getClass()).isAssignableFrom(HeaderRenderer.class);
        assertThat(renderers.get(1).getClass()).isAssignableFrom(BodyRenderer.class);
        assertThat(renderers.get(2).getClass()).isAssignableFrom(FooterRenderer.class);
    }

    @Test
    void renderer_has_beautiful_code() throws IOException {
        // given
        var beautifulCode = beautifulCode();
        var path = Paths.get("src/main/java/pillars/MessageRenderer.java");

        // when
        var codePath = Files.lines(path);
        var realCode = codePath.collect(Collectors.joining("\n"));
        codePath.close();

        // then
        assertThat(realCode).isEqualTo(beautifulCode);
    }

    static String beautifulCode() {
        return """
                package pillars;
                                
                import java.util.List;
                                
                public class MessageRenderer implements Renderer {
                    private final List<Renderer> subrenderers;
                                
                    public MessageRenderer() {
                        subrenderers = List.of(new HeaderRenderer(), new BodyRenderer(), new FooterRenderer());
                    }
                                
                    @Override
                    public String render(Message message) {
                        return subrenderers.stream()
                                .map(renderer -> renderer.render(message))
                                .reduce((s1, s2) -> s1 + s2)
                                .orElse("<error>Ooops</error");
                    }
                                
                    public List<Renderer> getSubrenderers() {
                        return subrenderers;
                    }
                }
                """.trim();
    }

}
