insert into company values(1, 'Emag', 100);
insert into company values(2, 'Altex', 50);
insert into company values(3, 'QuickMobile', 25);

insert into user_tbl (id, email, type, is_email_confirmed) values (1, 'gigi@emag.com', 'CUSTOMER', true);
insert into user_tbl (id, email, type, is_email_confirmed) values (2, 'vasile@emag.com', 'EMPLOYEE', true);
insert into user_tbl (id, email, type, is_email_confirmed) values (3, 'ion@altex.com', 'CUSTOMER', true);
