package refactor.model;

public enum ChangeEmailStatus {
    NOT_CONFIRMED,
    DUPLICATE,
    OK
}
