package refactor.model;

import javax.persistence.*;

@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String name;
    @Column(name = "number_of_employees")
    private Integer numberOfEmployees;

    public Company() {
    }

    public Company(Integer id, String name, Integer numberOfEmployees) {
        this.id = id;
        this.name = name;
        this.numberOfEmployees = numberOfEmployees;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(Integer numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public static String getCompanyNameFromEmail(String email) {
        var splitEmail = email.split("@")[1];
        var lastDotIndex = splitEmail.lastIndexOf('.');
        return splitEmail.substring(0, lastDotIndex);
    }

    public boolean isEmailCorporate(String newEmail) {
        return getCompanyNameFromEmail(newEmail).equalsIgnoreCase(this.name);
    }

    public void changeNumberOfEmployees(int delta) {
        if (delta + this.numberOfEmployees < 0) {
            throw new IllegalArgumentException(delta + " + " + this.numberOfEmployees + " cannot be < 0");
        }
        this.numberOfEmployees += delta;
    }
}