package refactor.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import refactor.bus.RabbitMQMessageBus;
import refactor.model.User;
import refactor.repository.CompanyRepository;
import refactor.repository.UserRepository;

@Controller
public class UserController {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final RabbitMQMessageBus bus;

    public UserController(UserRepository userRepository, CompanyRepository companyRepository, RabbitMQMessageBus bus) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.bus = bus;
    }

    public ResponseEntity<User> changeEmail(Integer userId, String newEmail) {
        var user = userRepository.findById(userId);

        if (!user.canChangeEmail()) {
            return ResponseEntity.badRequest().build();
        }
        var company = companyRepository.findByEmail(newEmail);
        user.changeEmail(newEmail, company);

        userRepository.update(user);
        companyRepository.update(company);

        for (var event : user.getEvents()) {
            bus.sendEmailChangedMessage(event.userId(), event.newEmail());
        }
        return ResponseEntity.ok(user);
    }
}
