package pollution;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ControllerTests {
    @Test
    void number_is_always_42() {
        var logger = new Logger(true);
        var sut = new Controller(logger);

        assertThat(sut.getNumber()).isEqualTo(42);
    }

}