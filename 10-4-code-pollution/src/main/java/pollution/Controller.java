package pollution;

public class Controller {
    private final Logger logger;

    public Controller(Logger logger) {
        this.logger = logger;
    }

    public int getNumber() {
        logger.log("logging number ");
        return 42;
    }
}
