package pollution;

public class Logger {
    private final boolean isTestEnvironment;

    public Logger(boolean isTestEnvironment) {
        this.isTestEnvironment = isTestEnvironment;
    }

    public void log(String message) {
        if (!isTestEnvironment) {
            System.out.println(message);
        }
    }
}
