package leaking;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTests {
    @Test
    void adding_two_numbers() {
        // given
        var calculator = new Calculator();

        // when
        var result = calculator.add(1, 2);

        // then
        assertThat(result).isEqualTo(1 + 2);
    }
}
