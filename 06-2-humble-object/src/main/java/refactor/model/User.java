package refactor.model;

public class User {
    private Integer id;
    private String email;
    private UserType type;

    public User(Integer id, String email, UserType type) {
        this.id = id;
        this.email = email;
        this.type = type;
    }

    public void changeEmail(String newEmail, Company company) {
        if (newEmail.equals(email)) {
            return;
        }

        var newType = company.isEmailCorporate(newEmail)
                ? UserType.EMPLOYEE
                : UserType.CUSTOMER;

        if (newType != this.type) {
            int delta = newType == UserType.EMPLOYEE ? 1 : -1;
            company.changeNumberOfEmployees(delta);
        }

        this.email = newEmail;
        this.type = newType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }
}