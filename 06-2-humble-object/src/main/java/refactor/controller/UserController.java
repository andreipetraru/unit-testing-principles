package refactor.controller;

import org.springframework.stereotype.Controller;
import refactor.database.Database;
import refactor.event.EventService;

@Controller
public class UserController {
    private final Database database;
    private final EventService eventService;

    public UserController(Database database, EventService eventService) {
        this.database = database;
        this.eventService = eventService;
    }

    public void changeEmail(Integer userId, String newEmail) {
        var user = database.getUserById(userId);
        var company = database.getCompanyByEmail(newEmail);

        user.changeEmail(newEmail, company);

        database.updateCompany(company);
        database.updateUser(user);

        eventService.sendMessage(user.getId(), user.getEmail());
    }
}
