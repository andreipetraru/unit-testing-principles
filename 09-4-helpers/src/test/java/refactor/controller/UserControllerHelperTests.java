package refactor.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import refactor.bus.Bus;
import refactor.bus.RabbitMQBus;
import refactor.bus.RabbitMQMessageBus;
import refactor.customassert.UserAssert;
import refactor.model.Company;
import refactor.model.User;
import refactor.model.UserType;
import refactor.repository.CompanyRepository;
import refactor.repository.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
@Sql(scripts = "/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class UserControllerHelperTests {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CompanyRepository companyRepository;
    UserController sut;
    Bus bus;

    @BeforeEach
    void init() {
        bus = mock(RabbitMQBus.class);
        var messageBus = new RabbitMQMessageBus(bus);
        sut = new UserController(userRepository, companyRepository, messageBus);
    }

    @Test
    void changing_email_from_corporate_to_non_corporate() {
        // given
        var user = new User();
        user.setEmail("alex@github.com");
        user.setEmailConfirmed(true);
        user.setType(UserType.CUSTOMER);
        userRepository.save(user);
        user = userRepository.findById(user.getId());

        var company = new Company();
        company.setNumberOfEmployees(1);
        company.setName("github");
        companyRepository.save(company);
        company = companyRepository.findById(company.getId());

        assertThat(user).isNotNull();
        assertThat(company).isNotNull();

        // when
        var userResponse = sut.changeEmail(user.getId(), "alex@emag.com");

        // then
        assertThat(userResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        var dbUser = userRepository.findById(user.getId());
        assertThat(dbUser).isEqualTo(userResponse.getBody());

        var dbCompany = companyRepository.findByEmail(dbUser.getEmail());
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(101);
        verify(bus, times(1)).send(dbUser.getId() + " has a new email address (alex@emag.com).");
        verifyNoMoreInteractions(bus);
    }

    @Test
    void changing_email_from_corporate_to_non_corporate_improved() {
        // given
        var user = createUser("alex@github.com");

        // when
        var userResponse = sut.changeEmail(user.getId(), "alex@emag.com");

        // then
        assertThat(userResponse.getStatusCode()).isEqualTo(HttpStatus.OK);

        var dbUser = userRepository.findById(user.getId());
        UserAssert.assertThat(dbUser)
                .hasEmail("alex@emag.com")
                .hasEmailConfirmed()
                .isEmployee();

        var dbCompany = companyRepository.findByEmail(dbUser.getEmail());
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(101);
        verify(bus, times(1)).send(dbUser.getId() + " has a new email address (alex@emag.com).");
        verifyNoMoreInteractions(bus);
    }

    User createUser(String email) {
        var user = new User();
        user.setEmail(email);
        user.setEmailConfirmed(true);
        user.setType(UserType.CUSTOMER);
        userRepository.save(user);
        return userRepository.findById(user.getId());
    }

    Company createCompany(String name) {
        var company = new Company();
        company.setNumberOfEmployees(1);
        company.setName(name);
        companyRepository.save(company);
        return companyRepository.findById(company.getId());
    }

}
