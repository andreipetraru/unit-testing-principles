package refactor.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.jdbc.Sql;
import refactor.bus.Bus;
import refactor.bus.RabbitMQMessageBus;
import refactor.mock.BusMock;
import refactor.repository.CompanyRepository;
import refactor.repository.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
@Sql(scripts = "/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class UserControllerTests {
    @Autowired
    UserRepository userRepository;
    @Autowired
    CompanyRepository companyRepository;
    RabbitMQMessageBus bus = mock(RabbitMQMessageBus.class);
    UserController sut;

    @BeforeEach
    void init() {
        sut = new UserController(userRepository, companyRepository, bus);
    }

    @Test
    void user_can_change_email() {
        // given
        var user = userRepository.findById(1);

        // when
        var result = sut.changeEmail(user.getId(), "alex@altex.ro");

        // then
        var dbUser = userRepository.findById(1);
        var dbCompany = companyRepository.findByEmail(dbUser.getEmail());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dbUser.getEmail()).isEqualTo("alex@altex.ro");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(51);
        verify(bus, times(1)).sendEmailChangedMessage(dbUser.getId(), dbUser.getEmail());
        verifyNoMoreInteractions(bus);
    }

    @Test
    void user_can_chang_email_with_edge_mock() {
        // given
        var bus = mock(Bus.class);
        var messageBus = new RabbitMQMessageBus(bus);
        var sut = new UserController(userRepository, companyRepository, messageBus);
        var user = userRepository.findById(1);

        // when
        var result = sut.changeEmail(user.getId(), "alex@altex.ro");

        // then
        var dbUser = userRepository.findById(1);
        var dbCompany = companyRepository.findByEmail(dbUser.getEmail());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dbUser.getEmail()).isEqualTo("alex@altex.ro");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(51);
        verify(bus, times(1)).send("1 has a new email address (alex@altex.ro).");
        verifyNoMoreInteractions(bus);
    }

    @Test
    void user_can_change_email_with_spy() {
        // given
        var bus = new BusMock();
        var rabbitBus = new RabbitMQMessageBus(bus);
        sut = new UserController(userRepository, companyRepository, rabbitBus);
        var user = userRepository.findById(1);

        // when
        var result = sut.changeEmail(user.getId(), "alex@altex.ro");

        // then
        var dbUser = userRepository.findById(1);
        var dbCompany = companyRepository.findByEmail(dbUser.getEmail());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dbUser.getEmail()).isEqualTo("alex@altex.ro");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(51);
        bus.assertNumberOfMessagesSent(1).withSendMessage(dbUser.getId(), "alex@altex.ro");
    }
}