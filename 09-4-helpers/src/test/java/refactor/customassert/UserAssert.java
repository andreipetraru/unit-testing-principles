package refactor.customassert;


import org.assertj.core.api.AbstractAssert;
import refactor.model.User;
import refactor.model.UserType;


public class UserAssert extends AbstractAssert<UserAssert, User> {
    public UserAssert(User actual) {
        super(actual, UserAssert.class);
    }

    public static UserAssert assertThat(User actual) {
        return new UserAssert(actual);
    }

    public UserAssert hasEmail(String email) {
        isNotNull();
        if (!email.equals(actual.getEmail())) {
            failWithMessage("Expected user to have email " + email + " but was " + actual.getEmail());
        }
        return this;
    }

    public UserAssert hasEmailConfirmed() {
        isNotNull();
        if (!actual.isEmailConfirmed()) {
            failWithMessage("Expected email to be confirmed");
        }
        return this;
    }

    public UserAssert isEmployee() {
        isNotNull();
        if (actual.getType() == UserType.CUSTOMER) {
            failWithMessage("Expected user to be employee");
        }
        return this;
    }
}
