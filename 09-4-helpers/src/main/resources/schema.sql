drop table if exists user_tbl;
drop table if exists company;

drop sequence if exists hibernate_sequence;
create sequence hibernate_sequence start 50;

create table user_tbl(
  id SERIAL,
  email varchar(255),
  type varchar(32),
  is_email_confirmed boolean,
  constraint pk_user primary key ( id )
);

create table company(
  id SERIAL,
  name varchar(255),
  number_of_employees numeric,
  constraint pk_org primary key ( id )
);