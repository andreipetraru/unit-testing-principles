package refactor.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "USER_TBL")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String email;
    @Enumerated(EnumType.STRING)
    private UserType type;
    @Column(name = "is_email_confirmed")
    private Boolean emailConfirmed;
    @Transient
    private List<EmailChangedEvent> events = new ArrayList<>();

    public User() {
    }

    public User(Integer id, String email, UserType type, boolean EmailConfirmed) {
        this.id = id;
        this.email = email;
        this.type = type;
        this.emailConfirmed = EmailConfirmed;
        this.events = new ArrayList<>();
    }

    public User(Integer id, String email, UserType type) {
        this(id, email, type, true);
    }

    public boolean canChangeEmail() {
        return emailConfirmed;
    }

    public ChangeEmailStatus changeEmail(String newEmail, Company company) {
        if (!canChangeEmail()) {
            return ChangeEmailStatus.NOT_CONFIRMED;
        }
        if (newEmail.equals(email)) {
            return ChangeEmailStatus.DUPLICATE;
        }

        var newType = company.isEmailCorporate(newEmail)
                ? UserType.EMPLOYEE
                : UserType.CUSTOMER;

        if (newType != this.type) {
            int delta = newType == UserType.EMPLOYEE ? 1 : -1;
            company.changeNumberOfEmployees(delta);
        }

        this.email = newEmail;
        this.type = newType;
        events.add(new EmailChangedEvent(id, newEmail));
        return ChangeEmailStatus.OK;
    }

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public UserType getType() {
        return type;
    }

    public Boolean isEmailConfirmed() {
        return emailConfirmed;
    }

    public List<EmailChangedEvent> getEvents() {
        return Collections.unmodifiableList(events);
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, emailConfirmed, id, type);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof User other))
            return false;
        return Objects.equals(email, other.email) && emailConfirmed == other.emailConfirmed
                && Objects.equals(id, other.id) && type == other.type;
    }
}