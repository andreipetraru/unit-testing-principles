package refactor.bus;

public interface Bus {
	void send(String message);
}
