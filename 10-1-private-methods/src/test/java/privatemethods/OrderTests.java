package privatemethods;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class OrderTests {
    @Test
    void generate_report_for_regular_customer() {
        // given
        var buyer = new Buyer("gigi");
        var ps5 = new Item("PlayStation 5", BigDecimal.valueOf(499.99));
        var xbox = new Item("Xbox Series X", BigDecimal.valueOf(399.99));
        var nintendoSwitch = new Item("Nintendo Switch", BigDecimal.valueOf(349.99));
        var order = new Order(buyer, List.of(ps5, xbox, nintendoSwitch));

        // when
        var price = order.generateReport();

        // then
        assertThat(price).isEqualTo("Customer name: gigi, total number of products: 3, total price: 1437.47");
    }

    @Test
    void generate_report_for_vip_customer() {
        // given
        var buyer = new Buyer("gigi");
        buyer.promote();
        var ps5 = new Item("PlayStation 5", BigDecimal.valueOf(499.99));
        var xbox = new Item("Xbox Series X", BigDecimal.valueOf(399.99));
        var nintendoSwitch = new Item("Nintendo Switch", BigDecimal.valueOf(349.99));
        var order = new Order(buyer, List.of(ps5, xbox, nintendoSwitch));

        // when
        var price = order.generateReport();

        // then
        assertThat(price).isEqualTo("Customer name: gigi, total number of products: 3, total price: 1293.72");
    }

}
