package privatemethods;

import privatemethods.service.VatService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class Order {
    private final Buyer buyer;
    private final List<Item> items;
    private final VatService vatService = new VatService();

    public Order(Buyer buyer, List<Item> items) {
        this.buyer = buyer;
        this.items = items;
    }

    public String generateReport() {
        return "Customer name: " + buyer.getName() + ", " +
                "total number of products: " + items.size() + ", " +
                "total price: " + calculatePrice();
    }

    private BigDecimal calculatePrice() {
        var price = BigDecimal.ZERO;
        for (var item : items) {
            var discount = item.price().multiply(buyer.getDiscount()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP);
            price = price.add(item.price().subtract(discount));
        }
        return price.add(price.multiply(vatService.getVat("en_US")).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP));
    }
}
