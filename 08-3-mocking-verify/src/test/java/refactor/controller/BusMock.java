package refactor.controller;

import refactor.bus.Bus;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BusMock implements Bus {
	private final List<String> messages = new ArrayList<>();

	@Override
	public void send(String message) {
		messages.add(message);
	}

	public BusMock assertNumberOfMessagesSent(int number) {
		assertThat(messages).hasSize(number);
		return this;
	}

	public BusMock withSendMessage(Integer userId, String newEmail) {
		var message = userId + " has a new email address (" + newEmail + ").";
		assertThat(messages).contains(message);
		return this;
	}

}
