package refactor.domain;

import org.junit.jupiter.api.Test;
import refactor.model.Company;
import refactor.model.User;
import refactor.model.UserType;

import static org.assertj.core.api.Assertions.assertThat;

class DomainTests {
    @Test
    void changing_email_from_non_corporate_to_corporate() {
        // given
        var company = new Company(1, "emag", 50);
        var sut = new User(1, "gigi@gmail.com", UserType.CUSTOMER);

        // when
        sut.changeEmail("gigi@emag.com", company);

        // then
        assertThat(company.getNumberOfEmployees()).isEqualTo(51);
        assertThat(sut.getEmail()).isEqualTo("gigi@emag.com");
        assertThat(sut.getType()).isEqualTo(UserType.EMPLOYEE);
    }

    @Test
    void changing_email_from_corporate_to_non_corporate() {
        // given
        var company = new Company(1, "emag", 50);
        var sut = new User(1, "gigi@emag.com", UserType.EMPLOYEE);

        // when
        sut.changeEmail("gigi@gmail.com", company);

        // then
        assertThat(company.getNumberOfEmployees()).isEqualTo(49);
        assertThat(sut.getEmail()).isEqualTo("gigi@gmail.com");
        assertThat(sut.getType()).isEqualTo(UserType.CUSTOMER);
    }

    @Test
    void changing_email_without_changing_user_type() {
        // given
        var company = new Company(1, "emag", 50);
        var sut = new User(1, "gigi@emag.com", UserType.EMPLOYEE);

        // when
        sut.changeEmail("gigi1@emag.com", company);

        // then
        assertThat(company.getNumberOfEmployees()).isEqualTo(50);
        assertThat(sut.getEmail()).isEqualTo("gigi1@emag.com");
        assertThat(sut.getType()).isEqualTo(UserType.EMPLOYEE);
    }

    @Test
    void changing_email_to_same_one() {
        // given
        var company = new Company(1, "emag", 50);
        var sut = new User(1, "gigi@gmail.com", UserType.EMPLOYEE);

        // when
        sut.changeEmail("gigi@gmail.com", company);

        // then
        assertThat(company.getNumberOfEmployees()).isEqualTo(50);
        assertThat(sut.getEmail()).isEqualTo("gigi@gmail.com");
        assertThat(sut.getType()).isEqualTo(UserType.EMPLOYEE);
    }

}