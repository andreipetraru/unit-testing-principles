package refactor.event;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import refactor.config.BeanConfigurator;

@Service
public class EventService {
    private final RabbitTemplate rabbitTemplate;

    public EventService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
    
    public void sendMessage(Integer userId, String newEmail) {
    	var message = userId + " has a new email address (" + newEmail + ").";
        rabbitTemplate.convertAndSend(BeanConfigurator.TOPIC_EXCHANGE_NAME, "foo.bar.baz", message);
    }

    public void receiveMessage(String message) {
        System.out.println("Received message " + message);
    }
}
