import mocks.store.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class GoodMockTests {
    Store store;
    CustomerRepository customerRepository;
    EmailService emailService;

    CustomerController sut;

    @BeforeEach
    void init() {
        customerRepository = new CustomerRepository();
        customerRepository.save(new Customer("1", "gigi", "gigi@gmail.com"));

        store = new Store();
        store.addInventory(Product.CONDITIONER, 10);

        emailService = mock(EmailService.class);

        sut = new CustomerController(customerRepository, store, emailService);
    }

    @Test
    void good_mock() {
        // when
        var purchaseSuccessful = sut.purchase("1", Product.CONDITIONER, 5);

        // then
        assertThat(purchaseSuccessful).isTrue();
        verify(emailService, times(1)).sendPurchaseEmail("gigi@gmail.com", Product.CONDITIONER, 5);
    }

    @Test
    @Disabled
    void bad_mock() {
        // given
        store = mock(Store.class);
        emailService = mock(EmailService.class);
        sut = new CustomerController(customerRepository, store, emailService);

        when(store.checkStock(Product.CONDITIONER)).thenReturn(10);

        // when
        var purchaseSuccessful = sut.purchase("1", Product.CONDITIONER, 5);

        // then
        assertThat(purchaseSuccessful).isTrue();
        verify(store, times(1)).removeInventory(true, Product.CONDITIONER, 5);
    }

}
