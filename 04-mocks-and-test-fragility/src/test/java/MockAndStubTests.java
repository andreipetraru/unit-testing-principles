import mocks.store.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class MockAndStubTests {
    CustomerRepository customerRepository;
    EmailService emailService;
    CustomerController sut;
    Store store;

    @BeforeEach
    void init() {
        emailService = mock(EmailService.class);
        customerRepository = new CustomerRepository();
        store = new Store();
        sut = new CustomerController(customerRepository, store, emailService);
    }

    @Test
    void mock_example() {
        // given
        var customer = new Customer("1", "gigi", "gigi@gmail.com");

        // when
        sut.createCustomer(customer);

        // then
        verify(emailService, times(1)).sendGreetingEmail("gigi@gmail.com");
    }

    @Test
    void stub_example() {
        // given
        customerRepository = mock(CustomerRepository.class);
        sut = new CustomerController(customerRepository, store, emailService);
        var mockedUser = new Customer("1", "gigi", "gigi@gmail.com");
        when(customerRepository.findById("1")).thenReturn(mockedUser);

        // when
        var user = sut.findById("1");

        // then
        assertThat(user).isEqualTo(mockedUser);
    }

    @Test
    void stub_example_with_verify() {
        // given
        customerRepository = mock(CustomerRepository.class);
        sut = new CustomerController(customerRepository, store, emailService);
        when(customerRepository.findById("1")).thenReturn(new Customer("1", "gigi", "gigi@gmail.com"));

        // when
        var user = sut.findById("1");

        // then
        assertThat(user.getName()).isEqualTo("gigi");
        verify(customerRepository, times(1)).findById("1");
    }

    @Test
    void stub_and_mock() {
        // given
        customerRepository = mock(CustomerRepository.class);
        sut = new CustomerController(customerRepository, store, emailService);
        var customer = new Customer("1", "gigi", "gigi@gmail.com");
        when(customerRepository.save(customer)).thenReturn(customer);

        // when
        var savedUser = sut.createCustomer(customer);

        // then
        assertThat(savedUser).isEqualTo(customer);
        verify(emailService, times(1)).sendGreetingEmail("gigi@gmail.com");
    }
}
