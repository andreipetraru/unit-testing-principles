package mocks.store;

public class EmailService {
    public void sendPurchaseEmail(String email, Product product, int quantity) {
        System.out.println(email + " purchased " + product + " ( " + quantity + " ).");
    }
    public void sendGreetingEmail(String email) {
        System.out.println("Welcome " + email);
    }
}
