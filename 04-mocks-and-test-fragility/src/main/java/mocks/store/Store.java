package mocks.store;

import java.util.EnumMap;
import java.util.Map;

public class Store {
    private final Map<Product, Integer> inventory;

    public Store() {
        this.inventory = new EnumMap<>(Product.class);
    }

    public void addInventory(Product product, Integer stock) {
        this.inventory.put(product, stock);
    }

    public void removeInventory(boolean success, Product product, Integer stock) {
        if (success) {
            inventory.put(product, stock);
        }
    }

    public Integer checkStock(Product product) {
        return inventory.get(product);
    }
}

