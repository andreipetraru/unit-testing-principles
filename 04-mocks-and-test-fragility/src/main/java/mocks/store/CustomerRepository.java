package mocks.store;

import java.util.HashMap;
import java.util.Map;

public class CustomerRepository {
    private final Map<String, Customer> customers = new HashMap<>();

    public Customer save(Customer customer) {
        customers.put(customer.getId(), customer);
        return customers.get(customer.getId());
    }

    public Customer findById(String id) {
        return customers.get(id);
    }
}
