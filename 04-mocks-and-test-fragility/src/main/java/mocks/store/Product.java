package mocks.store;

public enum Product {
    SHAMPOO,
    SOAP,
    CONDITIONER
}
