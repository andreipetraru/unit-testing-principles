package mocks.store;

public class CustomerController {
    private final CustomerRepository customerRepository;
    private final Store store;
    private final EmailService emailGateway;

    public CustomerController(CustomerRepository customerRepository, Store store, EmailService emailGateway) {
        this.customerRepository = customerRepository;
        this.store = store;
        this.emailGateway = emailGateway;
    }

    public boolean purchase(String customerId, Product product, int quantity) {
        var customer = customerRepository.findById(customerId);
        var purchaseSuccessful = customer.purchase(store, product, quantity);
        if (purchaseSuccessful) {
            emailGateway.sendPurchaseEmail(customer.getEmail(), product, quantity);
        }
        return purchaseSuccessful;
    }

    public Customer createCustomer(Customer customer) {
        customer = customerRepository.save(customer);
        emailGateway.sendGreetingEmail(customer.getEmail());
        return customer;
    }

    public Customer findById(String id) {
        return customerRepository.findById(id);
    }
}
