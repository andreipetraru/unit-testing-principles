package mocks.observable;

public class UserController {
    private final UserStore userStore;

    public UserController(UserStore userStore) {
        this.userStore = userStore;
    }

    public void renameUser(Integer id, String newName) {
        var user = userStore.getUser(id);
        var normalizedName = user.normalizeName(newName);
        user.setName(normalizedName);
        userStore.saveUser(user);
    }
}
