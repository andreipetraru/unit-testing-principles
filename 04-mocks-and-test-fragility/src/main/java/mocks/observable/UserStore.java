package mocks.observable;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class UserStore {
    private final AtomicInteger id = new AtomicInteger();
    private final Map<Integer, User> users;

    public UserStore() {
        this.users = new HashMap<>();
    }

    public void saveUser(User user) {
        if (user.getId() == null) {
            user.setId(id.incrementAndGet());
        }
        users.put(user.getId(), user);
    }

    public User getUser(Integer id) {
        return users.get(id);
    }
}
