package refactor;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import refactor.controller.UserController;
import refactor.database.Database;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
class UserControllerTests {
    @Autowired
    Database db;
    @Autowired
    UserController sut;

    @Test
    void user_can_change_email() {
        // given
        var user = db.getUserById(1);

        // when
        sut.changeEmail(user.getId(), "alex@altex.ro");

        // then
        var dbUser = db.getUserById(1);
        var dbCompany = db.getCompanyByEmail(dbUser.getEmail());
        assertThat(dbUser.getEmail()).isEqualTo("alex@altex.ro");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(51);
    }
}