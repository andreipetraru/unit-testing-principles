package refactor.model;

public class Company {
    private Integer id;
    private String name;
    private Integer numberOfEmployees;

    public Company(Integer id, String name, Integer numberOfEmployees) {
        this.id = id;
        this.name = name;
        this.numberOfEmployees = numberOfEmployees;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(Integer numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public static String getCompanyNameFromEmail(String email) {
        var splitEmail = email.split("@")[1];
        var lastDotIndex = splitEmail.lastIndexOf('.');
        return splitEmail.substring(0, lastDotIndex);
    }
}