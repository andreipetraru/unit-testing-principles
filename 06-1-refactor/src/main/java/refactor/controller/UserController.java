package refactor.controller;

import org.springframework.stereotype.Controller;
import refactor.database.Database;
import refactor.event.EventService;
import refactor.model.Company;
import refactor.model.UserType;

@Controller
public class UserController {
    private final Database database;
    private final EventService eventService;

    public UserController(Database database, EventService eventService) {
        this.database = database;
        this.eventService = eventService;
    }

    public void changeEmail(Integer userId, String newEmail) {
        var user = database.getUserById(userId);
        if (user.getEmail().equals(newEmail)) {
            return;
        }

        var companyName = Company.getCompanyNameFromEmail(newEmail);
        var company = database.getCompanyByName(companyName);
        var userType = UserType.CUSTOMER;
        if (company != null) {
            userType = UserType.EMPLOYEE;
        }
        if (user.getType() != userType) {
            int delta = userType == UserType.EMPLOYEE ? 1 : -1;
            int newNumberOfEmployees = company.getNumberOfEmployees() + delta;
            company.setNumberOfEmployees(newNumberOfEmployees);
            database.updateCompany(company);
        }

        user.setEmail(newEmail);
        user.setType(userType);
        database.updateUser(user);
        eventService.sendMessage(user.getId(), user.getEmail());
    }
}
