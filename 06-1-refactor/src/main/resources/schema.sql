create table user_tbl(
  id numeric,
  email varchar(255),
  type varchar(32),
  constraint pk_user primary key ( id )
);

create table company(
  id numeric,
  name varchar(255),
  number_of_employees numeric DEFAULT 10,
  constraint pk_org primary key ( id )
);