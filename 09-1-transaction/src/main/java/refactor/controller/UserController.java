package refactor.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.support.TransactionTemplate;
import refactor.bus.RabbitMQMessageBus;
import refactor.database.Database;
import refactor.model.User;

@Controller
public class UserController {
    private final Database database;
    private final RabbitMQMessageBus bus;
    private final TransactionTemplate transactionTemplate;

    public UserController(Database database, RabbitMQMessageBus bus, TransactionTemplate transactionTemplate) {
        this.database = database;
        this.bus = bus;
        this.transactionTemplate = transactionTemplate;
    }

    public ResponseEntity<User> changeEmailWithTransaction(Integer userId, String newEmail) {
        var user = database.getUserById(userId);

        if (!user.canChangeEmail()) {
            return ResponseEntity.badRequest().build();
        }
        var company = database.getCompanyByEmail(newEmail);
        user.changeEmail(newEmail, company);

        transactionTemplate.execute(status -> {
            database.updateUser(user);
            database.updateCompany(company);

            for (var event : user.getEvents()) {
                bus.sendEmailChangedMessage(event.userId(), event.newEmail());
            }
            return null;
        });
        return ResponseEntity.ok(user);
    }
}
