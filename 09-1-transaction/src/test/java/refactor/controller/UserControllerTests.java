package refactor.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.support.TransactionTemplate;
import refactor.bus.RabbitMQMessageBus;
import refactor.database.Database;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@SpringBootTest
@Disabled
class UserControllerTests {
    @Autowired
    Database db;
    RabbitMQMessageBus bus = mock(RabbitMQMessageBus.class);
    UserController sut;
    @Autowired
    TransactionTemplate transactionTemplate;

    @BeforeEach
    void init() {
        sut = new UserController(db, bus, transactionTemplate);
    }

    @Test
    @Transactional
    void user_can_change_email_with_test_transaction() {
        // given
        var user = db.getUserById(1);

        // when
        try {
            sut.changeEmailWithTransaction(user.getId(), "alex@altex.ro");
        }
        catch (Exception e) {
            System.out.println(e);
        }

        // then
        var dbUser = db.getUserById(1);
        var dbCompany = db.getCompanyByEmail(dbUser.getEmail());
        System.out.println("user email = " + dbUser.getEmail());
        System.out.println("company no. of employees = " + dbCompany.getNumberOfEmployees());
        assertThat(dbUser.getEmail()).isEqualTo("gigi@emag.com");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(100);
    }

    @Test
    void user_can_change_email_without_test_transaction() {
        // given
        var user = db.getUserById(1);

        // when
        try {
            sut.changeEmailWithTransaction(user.getId(), "alex@altex.ro");
        }
        catch (Exception e) {
            System.out.println(e);
        }

        // then
        var dbUser = db.getUserById(1);
        var dbCompany = db.getCompanyByEmail(dbUser.getEmail());
        System.out.println("user email = " + dbUser.getEmail());
        System.out.println("company no. of employees = " + dbCompany.getNumberOfEmployees());
        assertThat(dbUser.getEmail()).isEqualTo("gigi@emag.com");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(100);
    }

}