drop table if exists user_tbl;
drop table if exists company;

create table user_tbl(
  id numeric auto_increment,
  email varchar(255),
  type varchar(32),
  isEmailConfirmed boolean,
  constraint pk_user primary key ( id )
);

create table company(
  id numeric auto_increment,
  name varchar(255),
  number_of_employees numeric DEFAULT 10,
  constraint pk_org primary key ( id )
);