package refactor.mapper;

import org.springframework.jdbc.core.RowMapper;
import refactor.model.User;
import refactor.model.UserType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new User(
                rs.getInt("id"),
                rs.getString("email"),
                UserType.valueOf(rs.getString("type")),
                rs.getBoolean("isEmailConfirmed"));
    }
}
