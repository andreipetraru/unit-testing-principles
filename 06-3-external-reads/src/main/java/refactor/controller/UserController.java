package refactor.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import refactor.database.Database;
import refactor.event.EventService;
import refactor.model.ChangeEmailStatus;
import refactor.model.User;

@Controller
public class UserController {
    private final Database database;
    private final EventService eventService;

    public UserController(Database database, EventService eventService) {
        this.database = database;
        this.eventService = eventService;
    }

    public ResponseEntity<User> changeEmail(Integer userId, String newEmail) {
        var user = database.getUserById(userId);
        var company = database.getCompanyByEmail(newEmail);

        var changeEmailStatus = user.changeEmail(newEmail, company);
        if (changeEmailStatus == ChangeEmailStatus.NOT_CONFIRMED) {
            return ResponseEntity.badRequest().build();
        }

        database.updateCompany(company);
        database.updateUser(user);

        eventService.sendMessage(user.getId(), user.getEmail());
        return ResponseEntity.ok(user);
    }
}
