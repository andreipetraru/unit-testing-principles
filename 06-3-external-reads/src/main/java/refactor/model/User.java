package refactor.model;

public class User {
    private Integer id;
    private String email;
    private UserType type;
    private boolean isEmailConfirmed;

    public User(Integer id, String email, UserType type, boolean isEmailConfirmed) {
        this.id = id;
        this.email = email;
        this.type = type;
        this.isEmailConfirmed = isEmailConfirmed;
    }

    public User(Integer id, String email, UserType type) {
        this.id = id;
        this.email = email;
        this.type = type;
        this.isEmailConfirmed = true;
    }

    public ChangeEmailStatus changeEmail(String newEmail, Company company) {
        if (!isEmailConfirmed) {
            return ChangeEmailStatus.NOT_CONFIRMED;
        }

        if (newEmail.equals(email)) {
            return ChangeEmailStatus.DUPLICATE;
        }

        var newType = company.isEmailCorporate(newEmail)
                ? UserType.EMPLOYEE
                : UserType.CUSTOMER;

        if (newType != this.type) {
            int delta = newType == UserType.EMPLOYEE ? 1 : -1;
            company.changeNumberOfEmployees(delta);
        }

        this.email = newEmail;
        this.type = newType;
        return ChangeEmailStatus.OK;
    }

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public UserType getType() {
        return type;
    }

    public boolean isEmailConfirmed() {
        return isEmailConfirmed;
    }

}