package refactor.model;

public enum UserType {
    CUSTOMER,
    EMPLOYEE
}
