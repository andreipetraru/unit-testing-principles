package pillars;

import java.time.LocalDateTime;

public record Delivery(String name, LocalDateTime deliveryDate) {
}
