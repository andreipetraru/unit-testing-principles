package pillars;

public record Inventory(Product product, Integer inventory) {
}
