package pillars;

import java.time.LocalDateTime;

public class DeliveryService {
    public boolean isDeliveryValid(Delivery delivery) {
        return delivery.deliveryDate().isAfter(LocalDateTime.now());
    }
}
