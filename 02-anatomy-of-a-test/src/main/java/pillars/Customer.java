package pillars;

import java.util.EnumMap;
import java.util.Map;

public class Customer {
    private Map<Product, Integer> products;
    private String name;
    private String id;
    private String email;

    public Customer(String name) {
        this.products = new EnumMap<>(Product.class);
        this.name = name;
    }

    public boolean purchase(Store store, Product product, Integer stock) {
        if (store.checkStock(product) < stock) {
            return false;
        }
        products.put(product, stock);
        return true;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
