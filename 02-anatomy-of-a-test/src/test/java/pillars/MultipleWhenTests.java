package pillars;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MultipleWhenTests {
    @Test
    void purchaseProductTest() {
        // given
        var store = new Store();
        store.addInventory(Product.CONDITIONER, 10);
        var customer = new Customer("gigi");

        // when
        var purchaseSuccessful = customer.purchase(store, Product.CONDITIONER, 5);
        store.removeInventory(purchaseSuccessful, Product.CONDITIONER, 5);

        // then
        assertThat(purchaseSuccessful).isTrue();
        assertThat(store.checkStock(Product.CONDITIONER)).isEqualTo(5);
    }
}
