package pillars;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class NamingTests {

    @Test
    void IsDeliveryValidInvalidDateReturnsFalse() {
        // given
        var sut = new DeliveryService();
        var invalidDate = LocalDateTime.now().minusDays(1);
        var delivery = new Delivery("MacBook Air", invalidDate);

        // when
        var isValid = sut.isDeliveryValid(delivery);

        // then
        assertThat(isValid).isFalse();
    }

}
