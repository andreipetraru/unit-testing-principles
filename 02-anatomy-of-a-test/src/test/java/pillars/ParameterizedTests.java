package pillars;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDateTime;
import java.util.stream.Stream;

class ParameterizedTests {
    @DisplayName("Can detect an invalid delivery date")
    @ParameterizedTest(name = "{0} day(s) from now delivery is {1}")
    @MethodSource("provideDates")
    void can_detect_an_invalid_delivery_date(int daysFromNow, boolean expected) {
        // given
        var sut = new DeliveryService();
        var deliveryDate = LocalDateTime.now().plusDays(daysFromNow);
        var delivery = new Delivery("Catan", deliveryDate);

        // when
        var isValid = sut.isDeliveryValid(delivery);

        // then
        Assertions.assertThat(isValid).isEqualTo(expected);
    }

    static Stream<Arguments> provideDates() {
        return Stream.of(
                Arguments.of(-1, false),
                Arguments.of(0, false),
                Arguments.of(1, true),
                Arguments.of(2, true)
        );
    }
}
