package privatemethods.service;


import privatetests.Buyer;
import privatetests.Item;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class PriceService {
    private final VatService vatService;

    public PriceService(VatService vatService) {
        this.vatService = vatService;
    }

    public BigDecimal calculatePrice(Buyer buyer, List<Item> items) {
        var price = BigDecimal.ZERO;
        for (var item : items) {
            var discount = item.price().multiply(buyer.getDiscount()).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP);
            price = price.add(item.price().subtract(discount));
        }
        return price.add(price.multiply(vatService.getVat("en_US")).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP));
    }
}
