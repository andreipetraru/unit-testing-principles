package privatemethods.service;

import java.math.BigDecimal;

public class VatService {
    public BigDecimal getVat(String locale) {
        if (locale.equals("en_US")) {
            return BigDecimal.valueOf(15);
        }
        return BigDecimal.valueOf(24);
    }
}
