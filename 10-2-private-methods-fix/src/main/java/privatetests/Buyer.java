package privatetests;

import java.math.BigDecimal;

public class Buyer {
    private final String name;
    private UserType userType;

    public Buyer(String name) {
        this.name = name;
        this.userType = UserType.REGULAR;
    }

    public void promote() {
        this.userType = UserType.VIP;
    }

    public BigDecimal getDiscount() {
        return userType == UserType.REGULAR ? BigDecimal.ZERO : BigDecimal.valueOf(10);
    }

    public String getName() {
        return name;
    }
}
