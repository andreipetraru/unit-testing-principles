package privatetests;

import privatemethods.service.PriceService;
import privatemethods.service.VatService;

import java.util.List;

public class Order {
    private final Buyer buyer;
    private final List<Item> items;

    public Order(Buyer buyer, List<Item> items) {
        this.buyer = buyer;
        this.items = items;
    }

    public String generateReport() {
        var vatService = new VatService();
        var priceService = new PriceService(vatService);
        return "Customer name: " + buyer.getName() + ", " +
                "total number of products: " + items.size() + ", " +
                "total price: " + priceService.calculatePrice(buyer, items);
    }

}
