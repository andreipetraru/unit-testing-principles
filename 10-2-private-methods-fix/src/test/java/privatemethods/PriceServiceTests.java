package privatemethods;

import org.junit.jupiter.api.Test;
import privatetests.Buyer;
import privatetests.Item;
import privatemethods.service.PriceService;
import privatemethods.service.VatService;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class PriceServiceTests {
    @Test
    void calculate_price_for_regular_customer() {
        // given
        var vatService = new VatService();
        var sut = new PriceService(vatService);
        var buyer = new Buyer("gigi");
        buyer.promote();
        var ps5 = new Item("PlayStation 5", BigDecimal.valueOf(499.99));
        var xbox = new Item("Xbox Series X", BigDecimal.valueOf(399.99));
        var nintendoSwitch = new Item("Nintendo Switch", BigDecimal.valueOf(349.99));

        // when
        var price = sut.calculatePrice(buyer, List.of(ps5, xbox, nintendoSwitch));

        // then
        assertThat(price).isEqualTo(BigDecimal.valueOf(1293.72));
    }
}
