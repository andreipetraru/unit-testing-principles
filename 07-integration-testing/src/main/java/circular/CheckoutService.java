package circular;

public class CheckoutService {
	public void checkOut(int orderId) {
		var service = new ReportService();
		service.generateReport(orderId, this);
	}

	public void complete(Report report) {
		System.out.println("Check out complete for " + report);
	}
}
