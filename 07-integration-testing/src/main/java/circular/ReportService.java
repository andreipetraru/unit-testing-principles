package circular;

public class ReportService {
	public void generateReport(int orderId, CheckoutService checkoutService) {
		var report = new Report(orderId);
		checkoutService.complete(report);
	}
}
