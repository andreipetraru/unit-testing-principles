package enterprise.impl;

import enterprise.UserController;
import org.springframework.stereotype.Controller;
import refactor.model.User;

@Controller
public class UserControllerImpl implements UserController {
    private final UserFacadeImpl userFacade;

    public UserControllerImpl(UserFacadeImpl userFacade) {
        this.userFacade = userFacade;
    }

    public User findById(Integer id) {
        return userFacade.findById(id);
    }
}
