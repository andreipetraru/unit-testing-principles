package enterprise.impl;

import enterprise.UserService;
import org.springframework.stereotype.Service;
import refactor.model.User;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepositoryImpl userRepository;

    public UserServiceImpl(UserRepositoryImpl userRepository) {
        this.userRepository = userRepository;
    }

    public User findById(Integer id) {
        return userRepository.findById(id);
    }
}
