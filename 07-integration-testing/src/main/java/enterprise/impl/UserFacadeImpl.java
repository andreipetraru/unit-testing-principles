package enterprise.impl;

import enterprise.UserFacade;
import org.springframework.stereotype.Service;
import refactor.model.User;

@Service
public class UserFacadeImpl implements UserFacade {
    private final UserServiceImpl userService;

    public UserFacadeImpl(UserServiceImpl userService) {
        this.userService = userService;
    }

    public User findById(Integer id) {
        return userService.findById(id);
    }
}
