package enterprise.impl;

import enterprise.UserRepository;
import org.springframework.stereotype.Repository;
import refactor.database.Database;
import refactor.model.User;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final Database database;

    public UserRepositoryImpl(Database database) {
        this.database = database;
    }

    public User findById(Integer id) {
        return database.getUserById(id);
    }
}
