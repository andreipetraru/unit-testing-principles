package enterprise;

import refactor.model.User;

public interface UserFacade {
    User findById(Integer id);
}
