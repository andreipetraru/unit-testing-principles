package enterprise;

import refactor.model.User;

public interface UserService {
    User findById(Integer id);
}
