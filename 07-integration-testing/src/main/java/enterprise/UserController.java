package enterprise;

import refactor.model.User;

public interface UserController {
    User findById(Integer id);
}
