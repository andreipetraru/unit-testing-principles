package enterprise;

import refactor.model.User;

public interface UserRepository {
    User findById(Integer id);
}
