package refactor.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "USER_TBL")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String email;
    @Enumerated(EnumType.STRING)
    private UserType type;
    @Column(name = "is_email_confirmed")
    private Boolean isEmailConfirmed;
    @Transient
    private List<EmailChangedEvent> events = new ArrayList<>();

    public User() {
    }

    public User(Integer id, String email, UserType type, boolean isEmailConfirmed) {
        this.id = id;
        this.email = email;
        this.type = type;
        this.isEmailConfirmed = isEmailConfirmed;
        this.events = new ArrayList<>();
    }

    public User(Integer id, String email, UserType type) {
        this(id, email, type, true);
    }

    public boolean canChangeEmail() {
        return isEmailConfirmed;
    }

    public ChangeEmailStatus changeEmail(String newEmail, Company company) {
        if (!canChangeEmail()) {
            return ChangeEmailStatus.NOT_CONFIRMED;
        }
        if (newEmail.equals(email)) {
            return ChangeEmailStatus.DUPLICATE;
        }

        var newType = company.isEmailCorporate(newEmail)
                ? UserType.EMPLOYEE
                : UserType.CUSTOMER;

        if (newType != this.type) {
            int delta = newType == UserType.EMPLOYEE ? 1 : -1;
            company.changeNumberOfEmployees(delta);
        }

        this.email = newEmail;
        this.type = newType;
        events.add(new EmailChangedEvent(id, newEmail));
        return ChangeEmailStatus.OK;
    }

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public UserType getType() {
        return type;
    }

    public Boolean isEmailConfirmed() {
        return isEmailConfirmed;
    }

    public List<EmailChangedEvent> getEvents() {
        return Collections.unmodifiableList(events);
    }

}