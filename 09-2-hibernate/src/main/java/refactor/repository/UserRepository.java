package refactor.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import refactor.model.User;

import javax.persistence.EntityManager;

@Repository
public class UserRepository {
	private final EntityManager entityManager;

	public UserRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional
	public void save(User user) {
		entityManager.persist(user);
	}

	@Transactional
	public void update(User user) {
		entityManager.merge(user);
	}

	@Transactional(readOnly = true)
	public User findById(Integer id) {
		return entityManager.find(User.class, id);
	}
}
