package refactor.repository;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import refactor.model.Company;

import javax.persistence.EntityManager;

@Repository
public class CompanyRepository {
	private final EntityManager entityManager;

	public CompanyRepository(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Transactional
	public void save(Company company) {
		entityManager.persist(company);
	}

	@Transactional
	public void update(Company company) {
		entityManager.merge(company);
	}

	@Transactional(readOnly = true)
	public Company findById(Integer id) {
		return entityManager.find(Company.class, id);
	}

	@Transactional(readOnly = true)
	public Company findByEmail(String email) {
		var name = Company.getCompanyNameFromEmail(email);
		return findByName(name);
	}

	@Transactional(readOnly = true)
	public Company findByName(String name) {
		var cb = entityManager.getCriteriaBuilder();
		var cr = cb.createQuery(Company.class);
		var root = cr.from(Company.class);
		var query = cr.select(root).where(cb.equal(cb.upper(root.get("name")), name.toUpperCase()));
		return entityManager.createQuery(query).getSingleResult();
	}
}
