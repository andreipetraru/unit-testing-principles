package unit;

import coupled.AuditManagerCoupled;
import decoupled.AuditEntry;
import decoupled.AuditManagerDecoupled;
import decoupled.FileManager;
import functional.AuditManagerFunctional;
import functional.AuditService;
import functional.FunctionalWriter;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;

public class AuditClient {
    public static final String PATH = "";

    public static void main(String[] args) {

    }

    static void functional() throws IOException {
        var writer = new FunctionalWriter();
        var auditManager = new AuditManagerFunctional(3);
        var service = new AuditService(PATH, auditManager, writer);
        service.addRecord("gigi", LocalDate.now());
        service.addRecord("vasile", LocalDate.now());
        service.addRecord("ion", LocalDate.now());
        service.addRecord("mitica", LocalDate.now());
    }

    static void decoupled() throws IOException {
        var fileManager = new FileManager();
        var auditManagerDecoupled = new AuditManagerDecoupled(3, Path.of(PATH), fileManager);
        auditManagerDecoupled.addRecord(new AuditEntry("gigi", LocalDate.now()));
        auditManagerDecoupled.addRecord(new AuditEntry("vasile", LocalDate.now()));
        auditManagerDecoupled.addRecord(new AuditEntry("ion", LocalDate.now()));
        auditManagerDecoupled.addRecord(new AuditEntry("mitica", LocalDate.now()));
    }

    static void coupled() throws IOException {
        var auditManagerCoupled = new AuditManagerCoupled(3, PATH);
        auditManagerCoupled.addRecord("gigi", LocalDate.now());
        auditManagerCoupled.addRecord("vasile", LocalDate.now());
        auditManagerCoupled.addRecord("ion", LocalDate.now());
        auditManagerCoupled.addRecord("mitica", LocalDate.now());
    }
}
