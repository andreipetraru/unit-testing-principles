package styles.communication;

import java.util.ArrayList;
import java.util.List;

public class UserController {
    private final List<String> users;
    private final EmailService emailService;

    public UserController(EmailService emailService) {
        this.emailService = emailService;
        this.users = new ArrayList<>();
    }

    public boolean addUser(String user) {
        if (users.contains(user)) {
            return false;
        }
        this.users.add(user);
        this.emailService.sendEmail(user + " created");
        return true;
    }
}
