package styles.state;

import java.util.ArrayList;
import java.util.List;

public class ItemStore {
    private final List<String> items;

    public ItemStore() {
        this.items = new ArrayList<>();
    }

    public void addItem(String item) {
        items.add(item);
    }

    public List<String> getItems() {
        return List.copyOf(items);
    }
}
