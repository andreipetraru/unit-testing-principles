package functional;

import java.io.IOException;
import java.time.LocalDate;

public class AuditService {
    private final String directoryName;
    private final AuditManagerFunctional auditManager;
    private final FunctionalWriter writer;

    public AuditService(String directoryName, AuditManagerFunctional auditManager, FunctionalWriter writer) {
        this.directoryName = directoryName;
        this.auditManager = auditManager;
        this.writer = writer;
    }

    public void addRecord(String visitorName, LocalDate timeOfVisit) throws IOException {
        var files = writer.readDirectory(directoryName);
        var update = auditManager.addRecord(files, visitorName, timeOfVisit);
        writer.applyUpdate(directoryName, update);
    }
}
