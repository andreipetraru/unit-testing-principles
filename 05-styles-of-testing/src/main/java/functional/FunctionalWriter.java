package functional;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;

public class FunctionalWriter {
    public List<FileContent> readDirectory(String directoryName) throws IOException {
        try (var filePath = Files.walk(Path.of(directoryName))) {
            return filePath
                    .filter(Files::isRegularFile)
                    .sorted(Comparator.comparing(Path::getFileName))
                    .map(this::getFileContent)
                    .toList();
        }
    }

    private FileContent getFileContent(Path path) {
        List<String> lines;
        try {
            lines = Files.readAllLines(path);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return new FileContent(path.getFileName().toString(), lines);
    }

    public void applyUpdate(String directoryName, FileContent fileContent) throws IOException {
        var filePath = directoryName + File.separator + fileContent.fileName();
        Files.write(Path.of(filePath), fileContent.lines(), StandardCharsets.UTF_8);
    }
}
