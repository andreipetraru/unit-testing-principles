package functional;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class AuditManagerFunctional {
    private final int maxEntriesPerFile;

    public AuditManagerFunctional(int maxEntriesPerFile) {
        this.maxEntriesPerFile = maxEntriesPerFile;
    }

    public FileContent addRecord(List<FileContent> contents, String visitorName, LocalDate date) {
        var sortedContents = contents.stream()
                .sorted(Comparator.comparing(FileContent::fileName))
                .toList();
        var newRecord = visitorName + ";" + date;
        if (sortedContents.isEmpty()) {
            return new FileContent("audit_1.txt", List.of(newRecord));
        }

        var lastFile = sortedContents.get(contents.size() - 1);
        var lines = lastFile.lines();
        if (lines.size() < maxEntriesPerFile) {
            var updatedContents = Stream.concat(lines.stream(), Stream.of(newRecord))
                    .toList();
            return new FileContent(lastFile.fileName(), updatedContents);
        } else {
            var newIndex = contents.size() + 1;
            var newFileName = "audit_" + newIndex + ".txt";
            return new FileContent(newFileName, List.of(newRecord));
        }
    }
}
