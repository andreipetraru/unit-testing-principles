package functional;

import java.util.List;

public record FileContent(String fileName, List<String> lines) {
}
