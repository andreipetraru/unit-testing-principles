package decoupled;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.util.List;

public class FileManager {
    public List<Path> getPaths(Path path) throws IOException {
        List<Path> files;

        try (var filePath = Files.walk(path)) {
            files = filePath
                    .filter(Files::isRegularFile)
                    .sorted(Comparator.comparing(Path::getFileName))
                    .toList();
        }
        return files;
    }

    public void writeToFile(Path path, String content) throws IOException {
        Files.writeString(path, content + "\n", StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }

    public List<String> readAllLines(Path path) throws IOException {
        return Files.readAllLines(path);
    }
}
