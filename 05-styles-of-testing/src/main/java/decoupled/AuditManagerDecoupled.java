package decoupled;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AuditManagerDecoupled {
    private final int maxEntriesPerFile;
    private final Path directoryPath;
    private final FileManager fileManager;

    public AuditManagerDecoupled(int maxEntriesPerFile, Path directoryPath, FileManager fileManager) {
        this.maxEntriesPerFile = maxEntriesPerFile;
        this.directoryPath = directoryPath;
        this.fileManager = fileManager;
    }

    public void addRecord(AuditEntry auditRecord) throws IOException {
        var files = fileManager.getPaths(directoryPath);

        if (files.isEmpty()) {
            var newFile = Paths.get(directoryPath + File.separator + createFileName(1));
            fileManager.writeToFile(newFile, auditRecord.getAuditLog());
            return;
        }

        var latestAuditFile = files.get(files.size() - 1);
        var contents = fileManager.readAllLines(latestAuditFile);

        if (maxEntriesPerFile > contents.size()) {
            fileManager.writeToFile(latestAuditFile, auditRecord.getAuditLog());
        } else {
            var newName = createFileName(files.size() + 1);
            var newFile = Paths.get(this.directoryPath + File.separator + newName);
            fileManager.writeToFile(newFile, auditRecord.getAuditLog());
        }
    }

    private static String createFileName(int index) {
        return "audit_" + index + ".txt";
    }
}