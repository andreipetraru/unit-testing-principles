package decoupled;

import java.time.LocalDate;

public class AuditEntry {
    private final String auditLog;

    public AuditEntry(String name, LocalDate date) {
        this.auditLog = name + ";" + date;
    }

    public String getAuditLog() {
        return auditLog;
    }
}
