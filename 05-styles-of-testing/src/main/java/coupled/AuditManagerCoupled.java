package coupled;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

public class AuditManagerCoupled {
	
    private final int maxEntriesPerFile;
    private final String directoryName;

    public AuditManagerCoupled(int maxEntriesPerFile, String directoryName) {
        this.maxEntriesPerFile = maxEntriesPerFile;
        this.directoryName = directoryName;
    }

    public void addRecord(String visitorName, LocalDate timeOfVisit) throws IOException  {
        List<Path> files;

        try (var filePath = Files.walk(Paths.get(directoryName))) {
            files = filePath
                    .filter(Files::isRegularFile)
					.sorted(Comparator.comparing(Path::getFileName))
                    .toList();
        }

        var newRecord = String.join(";", visitorName, timeOfVisit.toString());

        if (files.isEmpty()) {
            var newFile = Paths.get(directoryName + File.separator + createFileName(1));
            Files.write(newFile, List.of(newRecord), StandardCharsets.UTF_8);
            return;
        }

        var latestAuditFile = files.get(files.size() - 1);
        var file = Paths.get(latestAuditFile.toAbsolutePath().toString());
        var contents = Files.readAllLines(file);

        if (maxEntriesPerFile > contents.size()) {
            contents.add(newRecord);
            Files.write(file, contents, StandardCharsets.UTF_8);
        } else {
            var newName = createFileName(files.size() + 1);
            var newFile = Paths.get(directoryName + File.separator + newName);
            Files.write(newFile, List.of(newRecord), StandardCharsets.UTF_8);
        }
    }

    private static String createFileName(int index) {
        return "audit_" + index + ".txt";
    }
}
