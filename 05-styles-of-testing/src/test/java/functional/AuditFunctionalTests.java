package functional;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class AuditFunctionalTests {
    AuditManagerFunctional sut = new AuditManagerFunctional(3);

    @Test
    void a_new_file_is_created_when_current_files_overflows() {
        // given
        var files = new FileContent("audit_1.txt",
                List.of("gigi;2020-12-12",
                        "gigi;2020-12-12",
                        "gigi;2020-12-12"));

        // when
        var update = sut.addRecord(List.of(files), "ion", LocalDate.of(2021, 1, 15));

        // then
        assertThat(update).isEqualTo(
                new FileContent("audit_2.txt",
                        List.of("ion;2021-01-15")));
    }

    @Test
    void a_new_file_is_created_when_dir_is_empty() {
        // when
        var update = sut.addRecord(List.of(), "ion", LocalDate.of(2021, 2, 14));

        // then
        assertThat(update).isEqualTo(
                new FileContent("audit_1.txt",
                        List.of("ion;2021-02-14")));
    }

    @Test
    void content_is_appended_to_latest_file_when_size_limit_is_not_reached() {
        // given
        var files = new FileContent("audit_1.txt",
                List.of("gigi;2020-12-12",
                        "gigi;2020-12-12"));

        // when
        var update = sut.addRecord(List.of(files), "ion", LocalDate.of(2021, 3, 21));

        // then
        assertThat(update).isEqualTo(
                new FileContent("audit_1.txt",
                        List.of("gigi;2020-12-12",
                                "gigi;2020-12-12",
                                "ion;2021-03-21")));
    }
}
