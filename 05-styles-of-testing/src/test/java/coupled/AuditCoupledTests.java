package coupled;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

class AuditCoupledTests {
    static final String TEST_PATH = "./src/test/resources";

    AuditManagerCoupled sut;

    @BeforeEach
    void init() throws IOException {
        var path = new File(TEST_PATH).getCanonicalPath();
        sut = new AuditManagerCoupled(3, path);
    }

    @AfterEach
    void cleanup() {
        Arrays.stream(Objects.requireNonNull(new File(TEST_PATH).listFiles()))
                .filter(file -> !file.isDirectory())
                .forEach(File::delete);
    }

    @Test
    void creates_audit_file_if_dir_is_empty() throws IOException {
        // when
        sut.addRecord("gigi", LocalDate.of(2000, 12, 25));

        // then
        checkNumberOfFiles(1);
        checkLatestEntry("audit_1.txt", "gigi;2000-12-25");
    }

    @Test
    void writes_to_latest_audit_file() throws IOException {
        // given
        addRecords(9);

        // when
        sut.addRecord("gigi", LocalDate.of(2005, 4, 20));

        // then
        checkNumberOfFiles(4);
        checkLatestEntry("audit_4.txt", "gigi;2005-04-20");
    }

    @Test
    void create_and_write_audit_when_limit_is_reached() throws IOException {
        // given
        addRecords(3);

        // when
        sut.addRecord("gigi", LocalDate.of(2005, 4, 20));

        // then
        checkNumberOfFiles(2);
        checkLatestEntry("audit_2.txt", "gigi;2005-04-20");

    }

    static void checkLatestEntry(String fileName, String content) throws IOException {
        var contents = Files.readAllLines(Path.of(TEST_PATH + "/" + fileName));
        assertThat(contents).last().isEqualTo(content);
    }

    static void checkNumberOfFiles(int expected) throws IOException {
        List<Path> files;
        try (var filePath = Files.walk(Paths.get(TEST_PATH))) {
            files = filePath
                    .filter(Files::isRegularFile)
                    .sorted((f1, f2) -> f1.getParent().compareTo(f2.getFileName()))
                    .toList();
        }
        assertThat(files).hasSize(expected);
    }

    void addRecords(int number) throws IOException {
        for (int i = 0; i < number; i++) {
            sut.addRecord("gigi", LocalDate.of(2000, 12, 25));
        }
    }

}
