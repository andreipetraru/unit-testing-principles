import org.junit.jupiter.api.Test;
import styles.communication.EmailService;
import styles.communication.UserController;
import styles.output.Calculator;
import styles.state.ItemStore;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class StyleTests {
    @Test
    void output_based_testing() {
        // given
        var sut = new Calculator();

        // when
        var result = sut.add(1, 2);

        // then
        assertThat(result).isEqualTo(3);
    }

    @Test
    void state_based_testing() {
        // given
        var itemStore = new ItemStore();

        // when
        itemStore.addItem("macbook");

        // then
        assertThat(itemStore.getItems()).first().isEqualTo("macbook");
    }

    @Test
    void communication_based_testing() {
        // given
        var emailService = mock(EmailService.class);
        var sut = new UserController(emailService);

        // when
        var userAdded = sut.addUser("gigi");

        // then
        assertThat(userAdded).isTrue();
        verify(emailService, times(1)).sendEmail("gigi created");
    }
}
