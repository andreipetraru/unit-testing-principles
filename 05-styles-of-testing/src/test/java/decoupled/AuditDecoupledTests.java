package decoupled;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;

import static org.mockito.Mockito.*;

class AuditDecoupledTests {
    FileManager fileManager;
    AuditManagerDecoupled sut;

    @BeforeEach
    void init() {
        fileManager = mock(FileManager.class);
        sut = new AuditManagerDecoupled(3, Path.of("audit"), fileManager);
    }

    @Test
    void create_new_audit_file_when_no_files_exist() throws IOException {
        // given
        when(fileManager.getPaths(Path.of("audit")))
                .thenReturn(List.of());

        var record = new AuditEntry("record", LocalDate.of(2001, 12, 30));

        // when
        sut.addRecord(record);

        // then
        verify(fileManager, times(1))
                .writeToFile(Path.of("audit/audit_1.txt"), "record;2001-12-30");
    }

    @Test
    void write_to_latest_audit_file_when_limit_is_not_reached() throws IOException {
        // given
        when(fileManager.getPaths(Path.of("audit")))
                .thenReturn(List.of(Path.of("audit/audit_1.txt")));
        when(fileManager.readAllLines(Path.of("audit/audit_1.txt")))
                .thenReturn(List.of("a;2000-12-12", "b;2000-12-12"));

        var record = new AuditEntry("record", LocalDate.of(2001, 12, 25));

        // when
        sut.addRecord(record);

        // then
        verify(fileManager, times(1))
                .writeToFile(Path.of("audit/audit_1.txt"), "record;2001-12-25");
    }

    @Test
    void create_and_write_to_new_audit_file_when_entry_limit_is_reached() throws IOException {
        // given
        when(fileManager.getPaths(Path.of("audit")))
                .thenReturn(List.of(
                        Path.of("audit/audit_1.txt"),
                        Path.of("audit/audit_2.txt")));
        when(fileManager.readAllLines(Path.of("audit/audit_2.txt")))
                .thenReturn(List.of("1", "2", "3"));

        var record = new AuditEntry("record", LocalDate.of(2000, 2, 15));

        // when
        sut.addRecord(record);

        // then
        verify(fileManager, times(1))
                .writeToFile(Path.of("audit/audit_3.txt"), "record;2000-02-15");
    }
}
