package coverage;

public class SideEffectCoverage {
    private static String lastLongString;

    public static boolean isStringLongWithEffects(String input) {
        if (input.length() > 5) {
            lastLongString = input;
            return true;
        }
        return false;
    }

    public static String getLastLongString() {
        return lastLongString;
    }
}
