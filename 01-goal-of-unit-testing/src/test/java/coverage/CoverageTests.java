package coverage;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CoverageTests {
    @Test
    void isStringLongTest() {
        assertThat(CodeCoverage.isStringLong("vasile")).isTrue();
    }

    @Test
    void isStringNotLongTest() {
        assertThat(FakeBranchCoverage.isStringLong("gigi")).isFalse();
    }

    @Test
    @Disabled
    void isStringMaybeLongTest() {
        assertThat(FakeBranchCoverage.isStringLong(null)).isFalse();
    }

    @Test
    void bestCoverageTest() {
        CodeCoverage.isStringLong("vasile");
        CodeCoverage.isStringLong("gigi");
    }

    @Test
    void side_effects() {
        assertThat(SideEffectCoverage.isStringLongWithEffects("gigi")).isFalse();
        assertThat(SideEffectCoverage.isStringLongWithEffects("vasile")).isTrue();
    }
}
