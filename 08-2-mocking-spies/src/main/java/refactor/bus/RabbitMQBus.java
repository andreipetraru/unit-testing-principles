package refactor.bus;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import refactor.config.BeanConfigurator;

@Service
public class RabbitMQBus implements Bus {
	private final RabbitTemplate rabbitTemplate;

	public RabbitMQBus(RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	public void send(String message) {
		rabbitTemplate.convertAndSend(BeanConfigurator.TOPIC_EXCHANGE_NAME, "foo.bar.baz", message);
	}
}
