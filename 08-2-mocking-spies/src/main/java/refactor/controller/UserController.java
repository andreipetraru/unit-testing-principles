package refactor.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import refactor.bus.RabbitMQMessageBus;
import refactor.database.Database;
import refactor.model.User;

@Controller
public class UserController {
    private final Database database;
    private final RabbitMQMessageBus bus;

    public UserController(Database database, RabbitMQMessageBus bus) {
        this.database = database;
        this.bus = bus;
    }

    public ResponseEntity<User> changeEmail(Integer userId, String newEmail) {
        var user = database.getUserById(userId);

        if (!user.canChangeEmail()) {
            return ResponseEntity.badRequest().build();
        }
        var company = database.getCompanyByEmail(newEmail);
        user.changeEmail(newEmail, company);

        database.updateCompany(company);
        database.updateUser(user);

        for (var event : user.getEvents()) {
            bus.sendEmailChangedMessage(event.userId(), event.newEmail());
        }
        return ResponseEntity.ok(user);
    }
}
