package refactor.model;

public record EmailChangedEvent(Integer userId, String newEmail) {
}
