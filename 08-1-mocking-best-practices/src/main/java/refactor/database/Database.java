package refactor.database;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import refactor.mapper.CompanyMapper;
import refactor.mapper.UserMapper;
import refactor.model.Company;
import refactor.model.User;

import java.sql.Types;

@Service
public class Database {
    public static final String GET_USER_BY_ID = "SELECT id, email, type, isEmailConfirmed from USER_TBL WHERE id = ?";
    public static final String UPDATE_USER = "UPDATE USER_TBL SET email = ?, type = ? WHERE id = ?";

    public static final String UPDATE_COMPANY = "UPDATE COMPANY SET name =?, number_of_employees = ? WHERE id = ?";
    public static final String GET_COMPANY_BY_NAME = "SELECT id, name, number_of_employees from COMPANY WHERE UPPER(name) = UPPER(?)";
    public static final String INSERT_NEW_COMPANY = "INSERT into COMPANY VALUES(default, ?, 1)";

    private final JdbcTemplate jdbcTemplate;

    public Database(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Company getCompanyByEmail(String email) {
        var name = Company.getCompanyNameFromEmail(email);
        return getCompanyByName(name);
    }

    public void updateUser(User user) {
        jdbcTemplate.update(UPDATE_USER, user.getEmail(), user.getType().toString(), user.getId());
    }

    public void updateCompany(Company company) {
        jdbcTemplate.update(UPDATE_COMPANY, company.getName(), company.getNumberOfEmployees(), company.getId());
    }

    public User getUserById(Integer userId) {
        return jdbcTemplate.queryForObject(GET_USER_BY_ID, new Object[] { userId }, new int[] { Types.INTEGER },
                new UserMapper());
    }

    public void createNewCompany(String name) {
        jdbcTemplate.update(INSERT_NEW_COMPANY, name);
    }

    public Company getCompanyByName(String companyName) {
        try {
            return jdbcTemplate.queryForObject(GET_COMPANY_BY_NAME, new Object[] { companyName },
                    new int[] { Types.VARCHAR }, new CompanyMapper());
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

}
