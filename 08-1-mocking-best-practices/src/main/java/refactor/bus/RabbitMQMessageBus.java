package refactor.bus;

import org.springframework.stereotype.Service;

@Service
public class RabbitMQMessageBus {
	private final RabbitMQBus bus;

	public RabbitMQMessageBus(RabbitMQBus bus) {
		this.bus = bus;
	}

	public void sendEmailChangedMessage(Integer userId, String newEmail) {
		bus.send(userId + " has a new email address (" + newEmail + ").");
	}

}
