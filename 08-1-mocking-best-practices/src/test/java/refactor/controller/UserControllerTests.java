package refactor.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import refactor.bus.RabbitMQMessageBus;
import refactor.database.Database;

import javax.transaction.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
@Transactional
class UserControllerTests {
    @Autowired
    Database db;
    RabbitMQMessageBus bus = mock(RabbitMQMessageBus.class);
    UserController sut;

    @BeforeEach
    void init() {
        sut = new UserController(db, bus);
    }

    @Test
    void user_can_change_email() {
        // given
        var user = db.getUserById(1);

        // when
        var result = sut.changeEmail(user.getId(), "alex@altex.ro");

        // then
        var dbUser = db.getUserById(1);
        var dbCompany = db.getCompanyByEmail(dbUser.getEmail());
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(dbUser.getEmail()).isEqualTo("alex@altex.ro");
        assertThat(dbCompany.getNumberOfEmployees()).isEqualTo(51);
        verify(bus, times(1)).sendEmailChangedMessage(dbUser.getId(), dbUser.getEmail());
    }
}